const express = require('express');

const app = express();
const port = process.env.PORT || 56201;

const parseRawBody = (req, res, next) => {
    req.setEncoding('utf8');
    req.rawBody = '';
    req.on('data', (chunk) => {
        req.rawBody += chunk;
    });
    req.on('end', () => {
        next();
    });
}
app.use(parseRawBody);

function reverseString(str) {
    return str.split('').reverse().join('');
}
const isLeap = year => new Date(year, 1, 29).getDate() === 29;

app.post('/square', async (req, res) => {
    const value = +req.rawBody;
    const result = {
        number: value,
        square: value * value
    }
    res.send(result);
});

app.post('/reverse', async (req, res) => {
    const value = req.rawBody;
    res.send(reverseString(value));
});

app.get('/date/:year/:month/:day', async (req, res) => {
    let { year, month, day } = req.params;
    month--;
    const date = new Date(year, month, day);
    const todayDate = new Date();
    const weekDayFullName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    const result = {
        weekDay: weekDayFullName[date.getDay()],
        isLeapYear: isLeap(year),
        difference: Math.abs(Math.floor((todayDate.getTime() - date.getTime()) / (1000 * 3600 * 24)))
    }
    res.send(result);
});

app.listen(port, () => {
    console.log('Server is up on port', port);
});
